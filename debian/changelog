facedetect (0.1-4) unstable; urgency=medium

  * Fix watch file
  * Standards-Version: 4.7.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 06 Dec 2024 08:23:14 +0100

facedetect (0.1-3) unstable; urgency=medium

  * Versioned Build-Depends opencv-data (data dir as moved from
    /usr/share/opencv to /usr/share/opencv4
    Closes: #856593
  * Do not Depend libopencv-dev (thanks for the hint to Dominik Stadler
    <dominik.stadler@gmx.at>)
    Closes: #929470
  * debhelper-compat 12
  * Standards-Version: 4.4.1
  * Secure URI in copyright format
  * Trim trailing whitespace.
  * Drop custom source compression.
  * Remove empty debian/source/options.
  * Set upstream metadata fields: Repository.
  * Upstream moved from github to gitlab

 -- Andreas Tille <tille@debian.org>  Mon, 04 Nov 2019 18:07:12 +0100

facedetect (0.1-2) unstable; urgency=medium

  * Use 2to3 to convert to Python3
  * s/python/python3/ in (Build-)Depends
  * Add dh-python to Build-Depends
  * Standards-Version: 4.1.3
  * Point Vcs fields to salsa.debian.org
  * Add example images
  * debhelper 11
  * Add autopkgtest

 -- Andreas Tille <tille@debian.org>  Fri, 06 Apr 2018 11:22:48 +0200

facedetect (0.1-1) unstable; urgency=medium

  * New upstream version
  * Upstream now uses tags -> remove get-orig-source, adapt watch
  * debhelper 10

 -- Andreas Tille <tille@debian.org>  Wed, 21 Dec 2016 22:53:32 +0100

facedetect (0.0.+20150428-1) unstable; urgency=medium

  * Initial release (Closes: #821247)

 -- Andreas Tille <tille@debian.org>  Sun, 17 Apr 2016 07:26:40 +0200
